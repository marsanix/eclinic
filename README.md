# eClinic

## Demo on Heroku

https://eclinic2017.herokuapp.com/

## Login Accounts

Admin
```
Username: admintesting
Password: Admin@2017
```

Doctor
```
Username: doctor
Password: Doctor@2017
```

User/Patient
```
Username: patient
Password: Patient@2017
```

## Database mLab

* To connect using the mongo shell:
```
mongo ds133241.mlab.com:33241/eclinic -u <dbuser> -p <dbpassword>
```
* To connect using a driver via the standard MongoDB URI (what's this?):
```
mongodb://<dbuser>:<dbpassword>@ds133241.mlab.com:33241/eclinic
```
* Database
```
Database: eclinic
Username: eclinic
Password: eClinic2017!~
```
