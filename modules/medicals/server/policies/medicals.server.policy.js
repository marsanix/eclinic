'use strict';

/**
 * Module dependencies
 */
var acl = require('acl');

// Using the memory backend
acl = new acl(new acl.memoryBackend());

/**
 * Invoke Medicals Permissions
 */
exports.invokeRolesPolicies = function () {
  acl.allow([{
    roles: ['admin'],
    allows: [{
      resources: '/api/medicals',
      permissions: '*'
    }, {
      resources: '/api/medicals/:medicalId',
      permissions: '*'
    }]
  }, {
    roles: ['doctor'],
    allows: [{
      resources: '/api/medicals',
      permissions: '*'
    }, {
      resources: '/api/medicals/:medicalId',
      permissions: '*'
    }]
  }, {
    roles: ['user'],
    allows: [{
      resources: '/api/medicals',
      permissions: ['get', 'post']
    }, {
      resources: '/api/medicals/:medicalId',
      permissions: ['get']
    }]
  }, {
    roles: ['guest'],
    allows: [{
      resources: '/api/medicals',
      permissions: ['get']
    }, {
      resources: '/api/medicals/:medicalId',
      permissions: ['get']
    }]
  }]);
};

/**
 * Check If Medicals Policy Allows
 */
exports.isAllowed = function (req, res, next) {
  var roles = (req.user) ? req.user.roles : ['guest'];

  // If an Medical is being processed and the current user created it then allow any manipulation
  if (req.medical && req.user && req.medical.user && req.medical.user.id === req.user.id) {
    return next();
  }

  // Check for user roles
  acl.areAnyRolesAllowed(roles, req.route.path, req.method.toLowerCase(), function (err, isAllowed) {
    if (err) {
      // An authorization error occurred
      return res.status(500).send('Unexpected authorization error');
    } else {
      if (isAllowed) {
        // Access granted! Invoke next middleware
        return next();
      } else {
        return res.status(403).json({
          message: 'User is not authorized'
        });
      }
    }
  });
};
