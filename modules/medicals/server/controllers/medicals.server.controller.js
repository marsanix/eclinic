 'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Medical = mongoose.model('Medical'),
  Doctor = mongoose.model('Doctor'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  _ = require('lodash');

/**
 * Create a Medical
 */
exports.create = function(req, res) {

  Doctor.findOne({user_doctor: req.user._id})
  .exec(function (d_err, doctor) {
    if (!d_err) {

      Medical.count().sort('-no').exec(function(e, c) {
        var mno = 1;
        if (!e) {
          mno = c + 1;
        } 

        var medical = new Medical(req.body);
        medical.user = req.user;
        medical.doctor = doctor;
        medical.no = mno;

        medical.save(function(err) {
          if (err) {
            return res.status(400).send({
              message: errorHandler.getErrorMessage(err)
            });
          } else {
            res.jsonp(medical);
          }
        });

      });

      return next();
    } 
  });  

  // var medical = new Medical(req.body);
  // medical.user = req.user;

  // medical.save(function(err) {
  //   if (err) {
  //     return res.status(400).send({
  //       message: errorHandler.getErrorMessage(err)
  //     });
  //   } else {
  //     res.jsonp(medical);
  //   }
  // });
};

/**
 * Show the current Medical
 */
exports.read = function(req, res) {
  // convert mongoose document to JSON
  var medical = req.medical ? req.medical.toJSON() : {};

  // Add a custom field to the Article, for determining if the current User is the "owner".
  // NOTE: This field is NOT persisted to the database, since it doesn't exist in the Article model.
  medical.isCurrentUserOwner = req.user && medical.user && medical.user._id.toString() === req.user._id.toString();

  res.jsonp(medical);
};

/**
 * Update a Medical
 */
exports.update = function(req, res) {
  var medical = req.medical;

  medical = _.extend(medical, req.body);

  medical.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(medical);
    }
  });
};

/**
 * Delete an Medical
 */
exports.delete = function(req, res) {
  var medical = req.medical;

  medical = _.extend(medical, {id: req.body.id,
                               is_deleted: true
                              });

  medical.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(medical);
    }
  });

  // medical.remove(function(err) {
  //   if (err) {
  //     return res.status(400).send({
  //       message: errorHandler.getErrorMessage(err)
  //     });
  //   } else {
  //     res.jsonp(medical);
  //   }
  // });
};

/**
 * List of Medicals
 */
exports.list = function(req, res) {
  Medical.find().sort('-created')
  .populate('patient', 'name')
  .populate('doctor', 'name')
  .populate('user', 'displayName')
  .exec(function(err, medicals) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(medicals);
    }
  });
};

/**
 * Medical middleware
 */
exports.medicalByID = function(req, res, next, id) {

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Medical is invalid'
    });
  }

  Medical.findById(id)
    .populate('patient', 'name')
    .populate('doctor', 'name')
    .populate('user', 'displayName')
    .exec(function (err, medical) {
    if (err) {
      return next(err);
    } else if (!medical) {
      return res.status(404).send({
        message: 'No Medical with that identifier has been found'
      });
    }
    req.medical = medical;
    next();
  });
};

exports.doctorByUserID = function(req, res, next, id) {

  Doctor.findOne({user_doctor: id}).exec(function (err, doctor) {
    if (err) {
      return next(err);
    } else if (!doctor) {
      return res.status(404).send({
        message: 'No Medical with that identifier has been found'
      });
    }
    req.doctor = doctor;
    next();
  });
};
