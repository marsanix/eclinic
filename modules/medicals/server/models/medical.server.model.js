'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Medical Schema
 */
var MedicalSchema = new Schema({
  no: {
    type: Number,
    required: true,
    unique: true
  },
  patient: {
    type: Schema.ObjectId,
    ref: 'Patient'
  },
  complined: {
    type: String,
    default: '',
    required: 'Please fill complined',
    trim: true
  },
  diagnosis: {
    type: String,
    default: '',
    required: 'Please fill diagnosis',
    trim: true
  },
  doctor: {
    type: Schema.ObjectId,
    ref: 'Doctor'
  },
  patient: {
    type: Schema.ObjectId,
    ref: 'Patient'
  },
  is_deleted: {
    type: Boolean,
    required: true,
    default: false
  },
  created: {
    type: Date,
    default: Date.now
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  }
});

mongoose.model('Medical', MedicalSchema);
