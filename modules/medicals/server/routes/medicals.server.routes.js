'use strict';

/**
 * Module dependencies
 */
var medicalsPolicy = require('../policies/medicals.server.policy'),
  medicals = require('../controllers/medicals.server.controller');

module.exports = function(app) {
  // Medicals Routes
  app.route('/api/medicals').all(medicalsPolicy.isAllowed)
    .get(medicals.list)
    .post(medicals.create);

  app.route('/api/medicals/:medicalId').all(medicalsPolicy.isAllowed)
    .get(medicals.read)
    .put(medicals.update)
    .delete(medicals.delete);

  // Finish by binding the Medical middleware
  app.param('medicalId', medicals.medicalByID);
};
