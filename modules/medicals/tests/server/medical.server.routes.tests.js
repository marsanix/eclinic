'use strict';

var should = require('should'),
  request = require('supertest'),
  path = require('path'),
  mongoose = require('mongoose'),
  User = mongoose.model('User'),
  Medical = mongoose.model('Medical'),
  express = require(path.resolve('./config/lib/express'));

/**
 * Globals
 */
var app,
  agent,
  credentials,
  user,
  medical;

/**
 * Medical routes tests
 */
describe('Medical CRUD tests', function () {

  before(function (done) {
    // Get application
    app = express.init(mongoose);
    agent = request.agent(app);

    done();
  });

  beforeEach(function (done) {
    // Create user credentials
    credentials = {
      username: 'username',
      password: 'M3@n.jsI$Aw3$0m3'
    };

    // Create a new user
    user = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'test@test.com',
      username: credentials.username,
      password: credentials.password,
      provider: 'local'
    });

    // Save a user to the test db and create new Medical
    user.save(function () {
      medical = {
        name: 'Medical name'
      };

      done();
    });
  });

  it('should be able to save a Medical if logged in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Medical
        agent.post('/api/medicals')
          .send(medical)
          .expect(200)
          .end(function (medicalSaveErr, medicalSaveRes) {
            // Handle Medical save error
            if (medicalSaveErr) {
              return done(medicalSaveErr);
            }

            // Get a list of Medicals
            agent.get('/api/medicals')
              .end(function (medicalsGetErr, medicalsGetRes) {
                // Handle Medicals save error
                if (medicalsGetErr) {
                  return done(medicalsGetErr);
                }

                // Get Medicals list
                var medicals = medicalsGetRes.body;

                // Set assertions
                (medicals[0].user._id).should.equal(userId);
                (medicals[0].name).should.match('Medical name');

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should not be able to save an Medical if not logged in', function (done) {
    agent.post('/api/medicals')
      .send(medical)
      .expect(403)
      .end(function (medicalSaveErr, medicalSaveRes) {
        // Call the assertion callback
        done(medicalSaveErr);
      });
  });

  it('should not be able to save an Medical if no name is provided', function (done) {
    // Invalidate name field
    medical.name = '';

    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Medical
        agent.post('/api/medicals')
          .send(medical)
          .expect(400)
          .end(function (medicalSaveErr, medicalSaveRes) {
            // Set message assertion
            (medicalSaveRes.body.message).should.match('Please fill Medical name');

            // Handle Medical save error
            done(medicalSaveErr);
          });
      });
  });

  it('should be able to update an Medical if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Medical
        agent.post('/api/medicals')
          .send(medical)
          .expect(200)
          .end(function (medicalSaveErr, medicalSaveRes) {
            // Handle Medical save error
            if (medicalSaveErr) {
              return done(medicalSaveErr);
            }

            // Update Medical name
            medical.name = 'WHY YOU GOTTA BE SO MEAN?';

            // Update an existing Medical
            agent.put('/api/medicals/' + medicalSaveRes.body._id)
              .send(medical)
              .expect(200)
              .end(function (medicalUpdateErr, medicalUpdateRes) {
                // Handle Medical update error
                if (medicalUpdateErr) {
                  return done(medicalUpdateErr);
                }

                // Set assertions
                (medicalUpdateRes.body._id).should.equal(medicalSaveRes.body._id);
                (medicalUpdateRes.body.name).should.match('WHY YOU GOTTA BE SO MEAN?');

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should be able to get a list of Medicals if not signed in', function (done) {
    // Create new Medical model instance
    var medicalObj = new Medical(medical);

    // Save the medical
    medicalObj.save(function () {
      // Request Medicals
      request(app).get('/api/medicals')
        .end(function (req, res) {
          // Set assertion
          res.body.should.be.instanceof(Array).and.have.lengthOf(1);

          // Call the assertion callback
          done();
        });

    });
  });

  it('should be able to get a single Medical if not signed in', function (done) {
    // Create new Medical model instance
    var medicalObj = new Medical(medical);

    // Save the Medical
    medicalObj.save(function () {
      request(app).get('/api/medicals/' + medicalObj._id)
        .end(function (req, res) {
          // Set assertion
          res.body.should.be.instanceof(Object).and.have.property('name', medical.name);

          // Call the assertion callback
          done();
        });
    });
  });

  it('should return proper error for single Medical with an invalid Id, if not signed in', function (done) {
    // test is not a valid mongoose Id
    request(app).get('/api/medicals/test')
      .end(function (req, res) {
        // Set assertion
        res.body.should.be.instanceof(Object).and.have.property('message', 'Medical is invalid');

        // Call the assertion callback
        done();
      });
  });

  it('should return proper error for single Medical which doesnt exist, if not signed in', function (done) {
    // This is a valid mongoose Id but a non-existent Medical
    request(app).get('/api/medicals/559e9cd815f80b4c256a8f41')
      .end(function (req, res) {
        // Set assertion
        res.body.should.be.instanceof(Object).and.have.property('message', 'No Medical with that identifier has been found');

        // Call the assertion callback
        done();
      });
  });

  it('should be able to delete an Medical if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Medical
        agent.post('/api/medicals')
          .send(medical)
          .expect(200)
          .end(function (medicalSaveErr, medicalSaveRes) {
            // Handle Medical save error
            if (medicalSaveErr) {
              return done(medicalSaveErr);
            }

            // Delete an existing Medical
            agent.delete('/api/medicals/' + medicalSaveRes.body._id)
              .send(medical)
              .expect(200)
              .end(function (medicalDeleteErr, medicalDeleteRes) {
                // Handle medical error error
                if (medicalDeleteErr) {
                  return done(medicalDeleteErr);
                }

                // Set assertions
                (medicalDeleteRes.body._id).should.equal(medicalSaveRes.body._id);

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should not be able to delete an Medical if not signed in', function (done) {
    // Set Medical user
    medical.user = user;

    // Create new Medical model instance
    var medicalObj = new Medical(medical);

    // Save the Medical
    medicalObj.save(function () {
      // Try deleting Medical
      request(app).delete('/api/medicals/' + medicalObj._id)
        .expect(403)
        .end(function (medicalDeleteErr, medicalDeleteRes) {
          // Set message assertion
          (medicalDeleteRes.body.message).should.match('User is not authorized');

          // Handle Medical error error
          done(medicalDeleteErr);
        });

    });
  });

  it('should be able to get a single Medical that has an orphaned user reference', function (done) {
    // Create orphan user creds
    var _creds = {
      username: 'orphan',
      password: 'M3@n.jsI$Aw3$0m3'
    };

    // Create orphan user
    var _orphan = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'orphan@test.com',
      username: _creds.username,
      password: _creds.password,
      provider: 'local'
    });

    _orphan.save(function (err, orphan) {
      // Handle save error
      if (err) {
        return done(err);
      }

      agent.post('/api/auth/signin')
        .send(_creds)
        .expect(200)
        .end(function (signinErr, signinRes) {
          // Handle signin error
          if (signinErr) {
            return done(signinErr);
          }

          // Get the userId
          var orphanId = orphan._id;

          // Save a new Medical
          agent.post('/api/medicals')
            .send(medical)
            .expect(200)
            .end(function (medicalSaveErr, medicalSaveRes) {
              // Handle Medical save error
              if (medicalSaveErr) {
                return done(medicalSaveErr);
              }

              // Set assertions on new Medical
              (medicalSaveRes.body.name).should.equal(medical.name);
              should.exist(medicalSaveRes.body.user);
              should.equal(medicalSaveRes.body.user._id, orphanId);

              // force the Medical to have an orphaned user reference
              orphan.remove(function () {
                // now signin with valid user
                agent.post('/api/auth/signin')
                  .send(credentials)
                  .expect(200)
                  .end(function (err, res) {
                    // Handle signin error
                    if (err) {
                      return done(err);
                    }

                    // Get the Medical
                    agent.get('/api/medicals/' + medicalSaveRes.body._id)
                      .expect(200)
                      .end(function (medicalInfoErr, medicalInfoRes) {
                        // Handle Medical error
                        if (medicalInfoErr) {
                          return done(medicalInfoErr);
                        }

                        // Set assertions
                        (medicalInfoRes.body._id).should.equal(medicalSaveRes.body._id);
                        (medicalInfoRes.body.name).should.equal(medical.name);
                        should.equal(medicalInfoRes.body.user, undefined);

                        // Call the assertion callback
                        done();
                      });
                  });
              });
            });
        });
    });
  });

  afterEach(function (done) {
    User.remove().exec(function () {
      Medical.remove().exec(done);
    });
  });
});
