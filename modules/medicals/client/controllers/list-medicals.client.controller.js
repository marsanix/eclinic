(function () {
  'use strict';

  angular
    .module('medicals')
    .controller('MedicalsListController', MedicalsListController);

  MedicalsListController.$inject = ['MedicalsService'];

  function MedicalsListController(MedicalsService) {
    var vm = this;

    vm.medicals = MedicalsService.query();
  }
}());
