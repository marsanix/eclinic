(function () {
  'use strict';

  // Medicals controller
  angular
    .module('medicals')
    .controller('MedicalsController', MedicalsController);

  MedicalsController.$inject = ['$scope', '$state', '$window', 'Authentication', 'medicalResolve', 'Notification', 'PatientsService'];

  function MedicalsController ($scope, $state, $window, Authentication, medical, Notification, PatientsService) {
    var vm = this;

    vm.authentication = Authentication;
    vm.medical = medical;
    vm.error = null;
    vm.form = {};
    vm.remove = remove;
    vm.save = save;

    PatientsService.query(function (data) {
      vm.patients = data;     
    });

    // Remove existing Medical
    function remove() {
      if ($window.confirm('Are you sure you want to delete?')) {
        vm.medical.$remove(function() {
          $state.go('medicals.list')
          Notification.success({ message: '<i class="glyphicon glyphicon-ok"></i> Medical record deleted successfully!' });
        });
      }
    }

    // Save Medical
    function save(isValid) {
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.form.medicalForm');
        return false;
      }

      // TODO: move create/update logic to service
      // if (vm.medical._id) {
      //   vm.medical.$update(successCallback, errorCallback);
      // } else {
      //   vm.medical.$save(successCallback, errorCallback);
      // }
      vm.medical.createOrUpdate()
        .then(successCallback)
        .catch(errorCallback);

      function successCallback(res) {
        $state.go('medicals.list');
        Notification.success({ message: '<i class="glyphicon glyphicon-ok"></i> Medical record saved successfully!' });
      }

      function errorCallback(res) {
        vm.error = res.data.message;
        Notification.error({ message: res.data.message, title: '<i class="glyphicon glyphicon-remove"></i> '+ res.data.message });
      }
    }
  }
}());
