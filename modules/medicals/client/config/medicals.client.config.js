(function () {
  'use strict';

  angular
    .module('medicals')
    .run(menuConfig);

  menuConfig.$inject = ['menuService'];

  function menuConfig(menuService) {
    // Set top bar menu items
    menuService.addMenuItem('topbar', {
      title: 'Medicals',
      state: 'medicals',
      type: 'dropdown',
      roles: ['user','doctor','admin']
    });

    // Add the dropdown list item
    menuService.addSubMenuItem('topbar', 'medicals', {
      title: 'List Medicals',
      state: 'medicals.list'
    });

    // Add the dropdown create item
    menuService.addSubMenuItem('topbar', 'medicals', {
      title: 'Create Medical',
      state: 'medicals.create',
      roles: ['doctor','admin']
    });
  }
}());
