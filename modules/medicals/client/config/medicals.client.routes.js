(function () {
  'use strict';

  angular
    .module('medicals')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('medicals', {
        abstract: true,
        url: '/medicals',
        template: '<ui-view/>'
      })
      .state('medicals.list', {
        url: '',
        templateUrl: 'modules/medicals/client/views/list-medicals.client.view.html',
        controller: 'MedicalsListController',
        controllerAs: 'vm',
        data: {
          pageTitle: 'Medicals List'
        }
      })
      .state('medicals.create', {
        url: '/create',
        templateUrl: 'modules/medicals/client/views/form-medical.client.view.html',
        controller: 'MedicalsController',
        controllerAs: 'vm',
        resolve: {
          medicalResolve: newMedical
        },
        data: {
          roles: ['doctor', 'admin'],
          pageTitle: 'Medicals Create'
        }
      })
      .state('medicals.edit', {
        url: '/:medicalId/edit',
        templateUrl: 'modules/medicals/client/views/form-medical.client.view.html',
        controller: 'MedicalsController',
        controllerAs: 'vm',
        resolve: {
          medicalResolve: getMedical
        },
        data: {
          roles: ['doctor', 'admin'],
          pageTitle: 'Edit Medical {{ medicalResolve.name }}'
        }
      })
      .state('medicals.view', {
        url: '/:medicalId',
        templateUrl: 'modules/medicals/client/views/view-medical.client.view.html',
        controller: 'MedicalsController',
        controllerAs: 'vm',
        resolve: {
          medicalResolve: getMedical
        },
        data: {
          pageTitle: 'Medical {{ medicalResolve.name }}'
        }
      });
  }

  getMedical.$inject = ['$stateParams', 'MedicalsService'];

  function getMedical($stateParams, MedicalsService) {
    return MedicalsService.get({
      medicalId: $stateParams.medicalId
    }).$promise;
  }

  newMedical.$inject = ['MedicalsService'];

  function newMedical(MedicalsService) {
    return new MedicalsService();
  }
}());
