(function (app) {
  'use strict';

  app.registerModule('medicals', ['ui.router', 'core.routes']);
}(ApplicationConfiguration));
