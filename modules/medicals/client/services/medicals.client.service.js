// Medicals service used to communicate Medicals REST endpoints
(function () {
  'use strict';

  angular
    .module('medicals')
    .factory('MedicalsService', MedicalsService);

  MedicalsService.$inject = ['$resource', '$log'];

  function MedicalsService($resource) {
    var Medical = $resource('/api/medicals/:medicalId', {
      medicalId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });

    angular.extend(Medical.prototype, {
      createOrUpdate: function () {
        var medical = this;
        return createOrUpdate(medical);
      }
    });

    return Medical;

    function createOrUpdate(medical) {
      if (medical._id) {
        return medical.$update(onSuccess, onError);
      } else {
        return medical.$save(onSuccess, onError);
      }

      // Handle successful response
      function onSuccess(medical) {
        // Any required internal processing from inside the service, goes here.
      }

      // Handle error response
      function onError(errorResponse) {
        var error = errorResponse.data;
        // Handle error internally
        handleError(error);
      }
    }

    function handleError(error) {
      // Log error
      $log.error(error);
    }

  }
}());
