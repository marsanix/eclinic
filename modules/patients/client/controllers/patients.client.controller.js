(function () {
  'use strict';

  // Patients controller
  angular
    .module('patients')
    .controller('PatientsController', PatientsController);

  PatientsController.$inject = ['$scope', '$state', '$window', 'Authentication', 'patientResolve', 'Notification'];

  function PatientsController ($scope, $state, $window, Authentication, patient, Notification) {
    var vm = this;

    vm.authentication = Authentication;
    vm.patient = patient;
    vm.error = null;
    vm.form = {};
    vm.remove = remove;
    vm.save = save;

    vm.patient.date_of_birth = new Date(moment(vm.patient.date_of_birth).format('MM-DD-YYYY'));

    // Remove existing Patient
    function remove() {
      if ($window.confirm('Are you sure you want to delete?')) {
        vm.patient.$remove(function() {
          $state.go('patients.list');
          Notification.success({ message: '<i class="glyphicon glyphicon-ok"></i> Patient deleted successfully!' });
        });
      }
    }

    // Save Patient
    function save(isValid) {
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.form.patientForm');
        return false;
      }

      // TODO: move create/update logic to service
      // if (vm.patient._id) {
      //   vm.patient.$update(successCallback, errorCallback);
      // } else {
      //   vm.patient.$save(successCallback, errorCallback);
      // }
      vm.patient.createOrUpdate()
        .then(successCallback)
        .catch(errorCallback);

      function successCallback(res) {
        $state.go('patients.list');
        Notification.success({ message: '<i class="glyphicon glyphicon-ok"></i> Patient saved successfully!' });
      }

      function errorCallback(res) {
        vm.error = res.data.message;
        Notification.error({ message: res.data.message, title: '<i class="glyphicon glyphicon-remove"></i> '+ res.data.message });
      }
    }
  }
}());
