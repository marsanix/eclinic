(function () {
  'use strict';

  angular
    .module('patients')
    .run(menuConfig);

  menuConfig.$inject = ['menuService'];

  function menuConfig(menuService) {
    // Set top bar menu items
    menuService.addMenuItem('topbar', {
      title: 'Patients',
      state: 'patients',
      type: 'dropdown',
      roles: ['doctor','admin']
    });

    // Add the dropdown list item
    menuService.addSubMenuItem('topbar', 'patients', {
      title: 'List Patients',
      state: 'patients.list'
    });

    // Add the dropdown create item
    menuService.addSubMenuItem('topbar', 'patients', {
      title: 'Create Patient',
      state: 'patients.create',
      roles: ['doctor','admin']
    });
  }
}());
