'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Patient Schema
 */
var PatientSchema = new Schema({
  no: {
    type: Number,
    unique: true,
    required: true
  },
  name: {
    type: String,
    default: '',
    required: 'Please fill Patient name',
    trim: true
  },
  place_of_birth: {
    type: String,
    default: '',
    trim: true
  },
  date_of_birth: {
    type: Date
  },
  gender: {
    type: String,
    enum: ['M', 'F'],
    required: 'Please fill Gender'
  },
  phone: {
    type: String,
    default: ''
  },
  address: {
    type: String,
    default: ''
  },
  is_deleted: {
    type: Boolean,
    required: true,
    default: false
  },
  created: {
    type: Date,
    default: Date.now
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  }
});

mongoose.model('Patient', PatientSchema);
