(function () {
  'use strict';

  angular
    .module('doctors')
    .run(menuConfig);

  menuConfig.$inject = ['menuService'];

  function menuConfig(menuService) {
    // Set top bar menu items
    menuService.addMenuItem('topbar', {
      title: 'Doctors',
      state: 'doctors',
      type: 'dropdown',
      roles: ['admin']
    });

    // Add the dropdown list item
    menuService.addSubMenuItem('topbar', 'doctors', {
      title: 'List Doctors',
      state: 'doctors.list'
    });

    // Add the dropdown create item
    menuService.addSubMenuItem('topbar', 'doctors', {
      title: 'Create Doctor',
      state: 'doctors.create',
      roles: ['admin']
    });
  }
}());
