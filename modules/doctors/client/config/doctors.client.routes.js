(function () {
  'use strict';

  angular
    .module('doctors')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('doctors', {
        abstract: true,
        url: '/doctors',
        template: '<ui-view/>'
      })
      .state('doctors.list', {
        url: '',
        templateUrl: 'modules/doctors/client/views/list-doctors.client.view.html',
        controller: 'DoctorsListController',
        controllerAs: 'vm',
        data: {
          pageTitle: 'Doctors List'
        }
      })
      .state('doctors.create', {
        url: '/create',
        templateUrl: 'modules/doctors/client/views/form-doctor.client.view.html',
        controller: 'DoctorsController',
        controllerAs: 'vm',
        resolve: {
          doctorResolve: newDoctor
        },
        data: {
          roles: ['admin'],
          pageTitle: 'Doctors Create'
        }
      })
      .state('doctors.edit', {
        url: '/:doctorId/edit',
        templateUrl: 'modules/doctors/client/views/form-doctor.client.view.html',
        controller: 'DoctorsController',
        controllerAs: 'vm',
        resolve: {
          doctorResolve: getDoctor
        },
        data: {
          roles: ['admin'],
          pageTitle: 'Edit Doctor {{ doctorResolve.name }}'
        }
      })
      .state('doctors.view', {
        url: '/:doctorId',
        templateUrl: 'modules/doctors/client/views/view-doctor.client.view.html',
        controller: 'DoctorsController',
        controllerAs: 'vm',
        resolve: {
          doctorResolve: getDoctor
        },
        data: {
          pageTitle: 'Doctor {{ doctorResolve.name }}'
        }
      });
  }

  getDoctor.$inject = ['$stateParams', 'DoctorsService'];

  function getDoctor($stateParams, DoctorsService) {
    return DoctorsService.get({
      doctorId: $stateParams.doctorId
    }).$promise;
  }

  newDoctor.$inject = ['DoctorsService'];

  function newDoctor(DoctorsService) {
    return new DoctorsService();
  }
}());
