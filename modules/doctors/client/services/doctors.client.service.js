// Doctors service used to communicate Doctors REST endpoints
(function () {
  'use strict';

  angular
    .module('doctors')
    .factory('DoctorsService', DoctorsService);

  DoctorsService.$inject = ['$resource', '$log'];

  function DoctorsService($resource) {
    var Doctor = $resource('/api/doctors/:doctorId', {
      doctorId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });

    angular.extend(Doctor.prototype, {
      createOrUpdate: function () {
        var doctor = this;
        return createOrUpdate(doctor);
      }
    });

    return Doctor;

    function createOrUpdate(doctor) {
      if (doctor._id) {
        return doctor.$update(onSuccess, onError);
      } else {
        return doctor.$save(onSuccess, onError);
      }

      // Handle successful response
      function onSuccess(doctor) {
        // Any required internal processing from inside the service, goes here.
      }

      // Handle error response
      function onError(errorResponse) {
        var error = errorResponse.data;
        // Handle error internally
        handleError(error);
      }
    }

    function handleError(error) {
      // Log error
      $log.error(error);
    }

  }
}());
