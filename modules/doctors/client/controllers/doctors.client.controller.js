(function () {
  'use strict';

  // Doctors controller
  angular
    .module('doctors')
    .controller('DoctorsController', DoctorsController);

  DoctorsController.$inject = ['$scope', '$state', '$window', 'Authentication', 'doctorResolve', 'Notification', 'AdminService'];

  function DoctorsController ($scope, $state, $window, Authentication, doctor, Notification, AdminService) {
    var vm = this;

    vm.authentication = Authentication;
    vm.doctor = doctor;
    vm.error = null;
    vm.form = {};
    vm.remove = remove;
    vm.save = save;

    AdminService.query(function (data) {
      vm.users = data;     
    });

    // Remove existing Doctor
    function remove() {
      if ($window.confirm('Are you sure you want to delete?')) {
        vm.doctor.$remove(function() {
          $state.go('doctors.list')
          Notification.success({ message: '<i class="glyphicon glyphicon-ok"></i> Doctor deleted successfully!' });
        });
      }
    }

    // Save Doctor
    function save(isValid) {
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.form.doctorForm');
        return false;
      }

      // TODO: move create/update logic to service
      // if (vm.doctor._id) {
      //   vm.doctor.$update(successCallback, errorCallback);
      // } else {
      //   vm.doctor.$save(successCallback, errorCallback);
      // }
      vm.doctor.createOrUpdate()
        .then(successCallback)
        .catch(errorCallback);

      function successCallback(res) {
        // $state.go('doctors.view', {
        //   doctorId: res._id
        // });
        $state.go('doctors.list');
        Notification.success({ message: '<i class="glyphicon glyphicon-ok"></i> Doctor saved successfully!' });
      }

      function errorCallback(res) {
        vm.error = res.data.message;
        Notification.error({ message: res.data.message, title: '<i class="glyphicon glyphicon-remove"></i> '+ res.data.message });
      }
    }
  }
}());
