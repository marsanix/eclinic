'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Doctor Schema
 */
var DoctorSchema = new Schema({
  no: {
    type: Number,
    unique: true,
    required: true
  },
  name: {
    type: String,
    default: '',
    required: 'Please fill Doctor name',
    trim: true
  },
  specialty: {
    type: String,
    default: '',
    required: 'Please fill the Specialty',
    trim: true
  },
  user_doctor: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  is_deleted: {
    type: Boolean,
    required: true,
    default: false
  },
  created: {
    type: Date,
    default: Date.now
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  }
});

mongoose.model('Doctor', DoctorSchema);
