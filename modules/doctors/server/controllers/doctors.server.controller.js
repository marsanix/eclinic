'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Doctor = mongoose.model('Doctor'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  _ = require('lodash');

/**
 * Create a Doctor
 */
exports.create = function(req, res) {

  Doctor.count().sort('-no').exec(function(e, c) {
    var dno = 1;
    if (!e) {
      dno = c + 1;
    } 

    var doctor = new Doctor(req.body);
    doctor.user = req.user;
    doctor.no = dno;

    doctor.save(function(err) {
      if (err) {
        return res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });
      } else {
        res.jsonp(doctor);
      }
    });

  });

  // var doctor = new Doctor(req.body);
  // doctor.user = req.user;

  // doctor.save(function(err) {
  //   if (err) {
  //     return res.status(400).send({
  //       message: errorHandler.getErrorMessage(err)
  //     });
  //   } else {
  //     res.jsonp(doctor);
  //   }
  // });
};

/**
 * Show the current Doctor
 */
exports.read = function(req, res) {
  // convert mongoose document to JSON
  var doctor = req.doctor ? req.doctor.toJSON() : {};

  // Add a custom field to the Article, for determining if the current User is the "owner".
  // NOTE: This field is NOT persisted to the database, since it doesn't exist in the Article model.
  doctor.isCurrentUserOwner = req.user && doctor.user && doctor.user._id.toString() === req.user._id.toString();

  res.jsonp(doctor);
};

/**
 * Update a Doctor
 */
exports.update = function(req, res) {
  var doctor = req.doctor;

  doctor = _.extend(doctor, req.body);

  doctor.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(doctor);
    }
  });
};

/**
 * Delete an Doctor
 */
exports.delete = function(req, res) {
  var doctor = req.doctor;

  doctor = _.extend(doctor, {id: req.body.id,
                             is_deleted: true
                             });

  doctor.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(doctor);
    }
  });

  // doctor.remove(function(err) {
  //   if (err) {
  //     return res.status(400).send({
  //       message: errorHandler.getErrorMessage(err)
  //     });
  //   } else {
  //     res.jsonp(doctor);
  //   }
  // });
};

/**
 * List of Doctors
 */
exports.list = function(req, res) {
  Doctor.find().sort('-created')
  .populate('user_doctor', 'displayName')
  .populate('user', 'displayName')
  .exec(function(err, doctors) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(doctors);
    }
  });
};

/**
 * Doctor middleware
 */
exports.doctorByID = function(req, res, next, id) {

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Doctor is invalid'
    });
  }

  Doctor.findById(id)
    .populate('user_doctor', 'displayName')
    .populate('user', 'displayName')
    .exec(function (err, doctor) {
    if (err) {
      return next(err);
    } else if (!doctor) {
      return res.status(404).send({
        message: 'No Doctor with that identifier has been found'
      });
    }
    req.doctor = doctor;
    next();
  });
};
