'use strict';

var should = require('should'),
  request = require('supertest'),
  path = require('path'),
  mongoose = require('mongoose'),
  User = mongoose.model('User'),
  Doctor = mongoose.model('Doctor'),
  express = require(path.resolve('./config/lib/express'));

/**
 * Globals
 */
var app,
  agent,
  credentials,
  user,
  doctor;

/**
 * Doctor routes tests
 */
describe('Doctor CRUD tests', function () {

  before(function (done) {
    // Get application
    app = express.init(mongoose);
    agent = request.agent(app);

    done();
  });

  beforeEach(function (done) {
    // Create user credentials
    credentials = {
      username: 'username',
      password: 'M3@n.jsI$Aw3$0m3'
    };

    // Create a new user
    user = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'test@test.com',
      username: credentials.username,
      password: credentials.password,
      provider: 'local'
    });

    // Save a user to the test db and create new Doctor
    user.save(function () {
      doctor = {
        name: 'Doctor name'
      };

      done();
    });
  });

  it('should be able to save a Doctor if logged in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Doctor
        agent.post('/api/doctors')
          .send(doctor)
          .expect(200)
          .end(function (doctorSaveErr, doctorSaveRes) {
            // Handle Doctor save error
            if (doctorSaveErr) {
              return done(doctorSaveErr);
            }

            // Get a list of Doctors
            agent.get('/api/doctors')
              .end(function (doctorsGetErr, doctorsGetRes) {
                // Handle Doctors save error
                if (doctorsGetErr) {
                  return done(doctorsGetErr);
                }

                // Get Doctors list
                var doctors = doctorsGetRes.body;

                // Set assertions
                (doctors[0].user._id).should.equal(userId);
                (doctors[0].name).should.match('Doctor name');

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should not be able to save an Doctor if not logged in', function (done) {
    agent.post('/api/doctors')
      .send(doctor)
      .expect(403)
      .end(function (doctorSaveErr, doctorSaveRes) {
        // Call the assertion callback
        done(doctorSaveErr);
      });
  });

  it('should not be able to save an Doctor if no name is provided', function (done) {
    // Invalidate name field
    doctor.name = '';

    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Doctor
        agent.post('/api/doctors')
          .send(doctor)
          .expect(400)
          .end(function (doctorSaveErr, doctorSaveRes) {
            // Set message assertion
            (doctorSaveRes.body.message).should.match('Please fill Doctor name');

            // Handle Doctor save error
            done(doctorSaveErr);
          });
      });
  });

  it('should be able to update an Doctor if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Doctor
        agent.post('/api/doctors')
          .send(doctor)
          .expect(200)
          .end(function (doctorSaveErr, doctorSaveRes) {
            // Handle Doctor save error
            if (doctorSaveErr) {
              return done(doctorSaveErr);
            }

            // Update Doctor name
            doctor.name = 'WHY YOU GOTTA BE SO MEAN?';

            // Update an existing Doctor
            agent.put('/api/doctors/' + doctorSaveRes.body._id)
              .send(doctor)
              .expect(200)
              .end(function (doctorUpdateErr, doctorUpdateRes) {
                // Handle Doctor update error
                if (doctorUpdateErr) {
                  return done(doctorUpdateErr);
                }

                // Set assertions
                (doctorUpdateRes.body._id).should.equal(doctorSaveRes.body._id);
                (doctorUpdateRes.body.name).should.match('WHY YOU GOTTA BE SO MEAN?');

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should be able to get a list of Doctors if not signed in', function (done) {
    // Create new Doctor model instance
    var doctorObj = new Doctor(doctor);

    // Save the doctor
    doctorObj.save(function () {
      // Request Doctors
      request(app).get('/api/doctors')
        .end(function (req, res) {
          // Set assertion
          res.body.should.be.instanceof(Array).and.have.lengthOf(1);

          // Call the assertion callback
          done();
        });

    });
  });

  it('should be able to get a single Doctor if not signed in', function (done) {
    // Create new Doctor model instance
    var doctorObj = new Doctor(doctor);

    // Save the Doctor
    doctorObj.save(function () {
      request(app).get('/api/doctors/' + doctorObj._id)
        .end(function (req, res) {
          // Set assertion
          res.body.should.be.instanceof(Object).and.have.property('name', doctor.name);

          // Call the assertion callback
          done();
        });
    });
  });

  it('should return proper error for single Doctor with an invalid Id, if not signed in', function (done) {
    // test is not a valid mongoose Id
    request(app).get('/api/doctors/test')
      .end(function (req, res) {
        // Set assertion
        res.body.should.be.instanceof(Object).and.have.property('message', 'Doctor is invalid');

        // Call the assertion callback
        done();
      });
  });

  it('should return proper error for single Doctor which doesnt exist, if not signed in', function (done) {
    // This is a valid mongoose Id but a non-existent Doctor
    request(app).get('/api/doctors/559e9cd815f80b4c256a8f41')
      .end(function (req, res) {
        // Set assertion
        res.body.should.be.instanceof(Object).and.have.property('message', 'No Doctor with that identifier has been found');

        // Call the assertion callback
        done();
      });
  });

  it('should be able to delete an Doctor if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Doctor
        agent.post('/api/doctors')
          .send(doctor)
          .expect(200)
          .end(function (doctorSaveErr, doctorSaveRes) {
            // Handle Doctor save error
            if (doctorSaveErr) {
              return done(doctorSaveErr);
            }

            // Delete an existing Doctor
            agent.delete('/api/doctors/' + doctorSaveRes.body._id)
              .send(doctor)
              .expect(200)
              .end(function (doctorDeleteErr, doctorDeleteRes) {
                // Handle doctor error error
                if (doctorDeleteErr) {
                  return done(doctorDeleteErr);
                }

                // Set assertions
                (doctorDeleteRes.body._id).should.equal(doctorSaveRes.body._id);

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should not be able to delete an Doctor if not signed in', function (done) {
    // Set Doctor user
    doctor.user = user;

    // Create new Doctor model instance
    var doctorObj = new Doctor(doctor);

    // Save the Doctor
    doctorObj.save(function () {
      // Try deleting Doctor
      request(app).delete('/api/doctors/' + doctorObj._id)
        .expect(403)
        .end(function (doctorDeleteErr, doctorDeleteRes) {
          // Set message assertion
          (doctorDeleteRes.body.message).should.match('User is not authorized');

          // Handle Doctor error error
          done(doctorDeleteErr);
        });

    });
  });

  it('should be able to get a single Doctor that has an orphaned user reference', function (done) {
    // Create orphan user creds
    var _creds = {
      username: 'orphan',
      password: 'M3@n.jsI$Aw3$0m3'
    };

    // Create orphan user
    var _orphan = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'orphan@test.com',
      username: _creds.username,
      password: _creds.password,
      provider: 'local'
    });

    _orphan.save(function (err, orphan) {
      // Handle save error
      if (err) {
        return done(err);
      }

      agent.post('/api/auth/signin')
        .send(_creds)
        .expect(200)
        .end(function (signinErr, signinRes) {
          // Handle signin error
          if (signinErr) {
            return done(signinErr);
          }

          // Get the userId
          var orphanId = orphan._id;

          // Save a new Doctor
          agent.post('/api/doctors')
            .send(doctor)
            .expect(200)
            .end(function (doctorSaveErr, doctorSaveRes) {
              // Handle Doctor save error
              if (doctorSaveErr) {
                return done(doctorSaveErr);
              }

              // Set assertions on new Doctor
              (doctorSaveRes.body.name).should.equal(doctor.name);
              should.exist(doctorSaveRes.body.user);
              should.equal(doctorSaveRes.body.user._id, orphanId);

              // force the Doctor to have an orphaned user reference
              orphan.remove(function () {
                // now signin with valid user
                agent.post('/api/auth/signin')
                  .send(credentials)
                  .expect(200)
                  .end(function (err, res) {
                    // Handle signin error
                    if (err) {
                      return done(err);
                    }

                    // Get the Doctor
                    agent.get('/api/doctors/' + doctorSaveRes.body._id)
                      .expect(200)
                      .end(function (doctorInfoErr, doctorInfoRes) {
                        // Handle Doctor error
                        if (doctorInfoErr) {
                          return done(doctorInfoErr);
                        }

                        // Set assertions
                        (doctorInfoRes.body._id).should.equal(doctorSaveRes.body._id);
                        (doctorInfoRes.body.name).should.equal(doctor.name);
                        should.equal(doctorInfoRes.body.user, undefined);

                        // Call the assertion callback
                        done();
                      });
                  });
              });
            });
        });
    });
  });

  afterEach(function (done) {
    User.remove().exec(function () {
      Doctor.remove().exec(done);
    });
  });
});
